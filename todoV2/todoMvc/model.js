(() => {
    function ToDo({ checked, text }) {
        return {
            checked,
            text
        };
    }

    function checkToDo({ toDo }) {
        return ToDo({ checked: true, text: toDo.text });
    }

    function uncheckToDo({ toDo }) {
        return ToDo({ checked: false, text: toDo.text });
    }

    function ToDos({ toDoList }) {
        return {
            toDoList
        };
    }

    function addToDo({ toDos, toDo }) {
        return ToDos({ toDoList: toDos.toDoList.concat(toDo) });
    }

    function deleteToDo({ toDos, index }) {
        return ToDos({
            toDoList: toDos.toDoList
                .slice(0, index)
                .concat(toDos.toDoList.slice(index + 1))
        });
    }

    function checkToDoInToDos({ toDos, index }) {
        return actionOnToDo({ toDos, index, toDoAction: checkToDo });
    }

    function uncheckToDoInToDos({ toDos, index }) {
        return actionOnToDo({
            toDos,
            index,
            toDoAction: uncheckToDo
        });
    }

    function actionOnToDo({ toDos, index, toDoAction }) {
        return ToDos({
            toDoList: toDos.toDoList
                .slice(0, index)
                .concat(toDoAction({ toDo: toDos.toDoList[index] }))
                .concat(toDos.toDoList.slice(index + 1))
        });
    }

    window.TodoModel = {
        ToDo,
        checkToDo,
        uncheckToDo,
        ToDos,
        addToDo,
        deleteToDo,
        checkToDoInToDos,
        uncheckToDoInToDos,
        actionOnToDo
    };
})();