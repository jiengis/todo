const testo = document.getElementById('testo')
const addButton = document.getElementById('add')
const todoList = document.getElementById('todoList')
let idDiv = 0

addButton.addEventListener('click',newTodo)

let myTodo = {
    newTodo : null
}

function newTodo(){
    if(myTodo.newTodo === null)
    createTodo()
}

function createTodo(){
    if(!canAdd()){
        alert('You must write something..!')
    }else{
        const newId = idDiv++;
        const todoElement = todoComponent(newId);
        todoList.append(todoElement)
        resetTodo()
    }
}

function todoComponent(id){
    const div = document.createElement('div');
    div.id = id
    const textElement = textComponent(testo.value);
    const checkElement = checkComponent()
    const deleteElement = deleteComponent()
    deleteElement.addEventListener('click', ()=> {
        deleteTodo(div)
    })
    checkElement.addEventListener('click', ()=> {
        enableDelete(checkElement,deleteElement)
    })
    div.append(textElement)
    div.append(checkElement)
    div.append(deleteElement)
    return div;
}

function textComponent(text){
    const span = document.createElement('span');
    span.innerText = text;
    return span;
}

function checkComponent(){
    const checkBox = document.createElement('input')
    checkBox.type = 'checkbox'
    return checkBox;
}

function enableDelete(checkbox,deleteElement){
    if(checkBoxStatus(checkbox) === true){
        showDelete(deleteElement)
    }
}

function showDelete(deleteButton){
    deleteButton.style.display = 'inline-block'
}

function checkBoxStatus(checkbox){
    if(checkbox.checked === true){
        return true;
    }else{
        return false;
    }
}

function deleteComponent(){
    const deleteButton = document.createElement('button')
    deleteButton.append('X')
    deleteButton.style.display = 'none';
    return deleteButton;
}

function deleteTodo(todo){
   todoList.removeChild(todo)
}

function canAdd(){
    return testo.value !== '';
}

function resetTodo(){
    testo.value = ''
}
